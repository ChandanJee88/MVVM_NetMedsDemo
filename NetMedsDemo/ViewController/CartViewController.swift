//
//  CartViewController.swift
//  NetMedsDemo
//
//  Created by Maa on 04/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import UIKit

class CartViewController: UIViewController {
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var tableViewCart: UITableView!
    let sqliteDbStore = SqliteDbStore()
    let cartviewModel = CartViewModel()
    var arrayGrandTotal = [Int]()
    @IBOutlet weak var lblTotal: UILabel!
    @IBOutlet weak var lblTotalPrice: UILabel!
    var userName : NSMutableArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //      let list = try? sqliteDbStore.readDataList()
        //        print(list)
        fetchCartList()
        self.tableViewCart.register(UINib(nibName: "CartTableCell", bundle: nil), forCellReuseIdentifier: "CartTableCell")
        totalSum()
        // Do any additional setup after loading the view.
    }
    
    func totalSum (){
        arrayGrandTotal.removeAll()
        for items in cartviewModel.cartList{
            guard let arr = items["minPrice"] as? String else { return  }
            arrayGrandTotal.append(Int(arr)!)
        }
        var sum = 0
        for num in arrayGrandTotal
        {
            sum +=  num
        }
        self.lblTotalPrice.text =  String(format: "%ld",sum )
    }
    
    @IBAction func tapToBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @objc func btnAction(_ sender: UIButton) {
        let row = sender.tag;
        print(row)
        let indexPath = IndexPath(row: row, section: 0)
        let cell = tableViewCart.dequeueReusableCell(withIdentifier: "CartTableCell", for: indexPath) as! CartTableCell
                
        let lists = cartviewModel.cartList[indexPath.row]
        if let list = lists as? [String: Any]{
            let id = list["itemId"] as! String
            print("Deleted ID:-", id)
            sqliteDbStore.delete(itemId: id)
            
        }
        
        fetchCartList()
        totalSum()
        self.tableViewCart.reloadData()
        
    }
    
}
extension CartViewController{
    func fetchCartList() {
        cartviewModel.fethDataFromDB()
    }
}

extension CartViewController:UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return  cartviewModel.cartList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CartTableCell", for: indexPath as IndexPath) as? CartTableCell else {
            fatalError("CartTable cell is not found")
        }
        let lists = cartviewModel.cartList[indexPath.row]
        cell.Item = lists
        cell.RemoveButton.tag = indexPath.row
        cell.RemoveButton.addTarget(self, action: #selector(self.btnAction(_:)), for: .touchUpInside)
        
        return cell
    }
}



