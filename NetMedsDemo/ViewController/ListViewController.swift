//
//  ListViewController.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import UIKit
var checkArray : NSMutableArray = NSMutableArray()

class ListViewController: UIViewController {
    
    
    let viewModel = ListViewModel()
    @IBOutlet weak var tableViews: UITableView!
    var filteredTableData = [ListTest]()
    var resultSearchController = UISearchController()
    var userName : NSMutableArray = NSMutableArray()
    let sqliteDbStore = SqliteDbStore()

    @IBOutlet weak var btnNext : UIButton!
    var i = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        fetchList()
        fetchToggleStatus()
        self.tableViews.register(UINib(nibName: "ListTableCell", bundle: nil), forCellReuseIdentifier: "ListTableCell")
        self.resultSearchController = ({
            let controller = UISearchController(searchResultsController: nil)
            //            controller.searchResultsUpdater = self
            //            controller.dimsBackgroundDuringPresentation = false
            controller.searchBar.sizeToFit()
            controller.searchBar.delegate = self
            controller.searchBar.placeholder = "Search by item name"
            controller.searchBar.barStyle = UIBarStyle.black
            controller.searchBar.barTintColor = UIColor.white
            controller.searchBar.backgroundColor = UIColor.clear
            self.tableViews.tableHeaderView = controller.searchBar
            return controller
        })()
        self.tableViews.reloadData()
        
        prepareViewModelObserver()
        // Do any additional setup after loading the view.
    }
    
    @objc func btnAddAction(_ sender: UIButton) {
        let row = sender.tag;
        print(row)
        let indexPath = IndexPath(row: row, section: 0)
        let cell: ListTableCell = tableViews.dequeueReusableCell(withIdentifier: "ListTableCell", for: indexPath) as! ListTableCell
        
        let model = self.userName[indexPath.row] as! addRemoveModel
        let id = viewModel.list[indexPath.row]
        if (model.is_check) {
            
            model.is_check = false
            cell.addRemoveButton.setTitle("Add", for: .normal)
            
            checkArray.remove(model.itemId)
            print("Deleting item Id:- ",id.itemId!)
            sqliteDbStore.deleteButton(itemId: id.itemId!)
            sqliteDbStore.delete(itemId: id.itemId!)
            if checkArray.count > 0 {
                print(checkArray.count)
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
            } else {
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
            }
        }else {
            model.is_check = true
            cell.addRemoveButton.setTitle("Remove", for: .normal)
            checkArray.add(model.itemId)
            
            if self.resultSearchController.isActive {
                let lists = filteredTableData[indexPath.row]
                sqliteDbStore.insertDataList(Sno: String(lists.Sno ?? 0), itemId: lists.itemId ?? "", itemName: lists.itemName ?? "", type: lists.type ?? "", Keyword: lists.Keyword ?? "", Best_sellers: lists.Best_sellers ?? "", testCount: String(lists.testCount ?? 0), IncludedTests: lists.IncludedTests ?? "", url: lists.url ?? "", minPrice: String(lists.minPrice ?? 0), labName: lists.labName ?? "", fasting: String(lists.fasting ?? 0), availableAt: String(lists.availableAt ?? 0), popular: lists.popular ?? "", category: lists.category ?? "", objectID: lists.objectID ?? "")
                 sqliteDbStore.update(is_check: 1, itemId: lists.itemId ?? "" )
            } else {
                let lists = viewModel.list[indexPath.row]
                sqliteDbStore.insertDataList(Sno: String(lists.Sno ?? 0), itemId: lists.itemId ?? "", itemName: lists.itemName ?? "", type: lists.type ?? "", Keyword: lists.Keyword ?? "", Best_sellers: lists.Best_sellers ?? "", testCount: String(lists.testCount ?? 0), IncludedTests: lists.IncludedTests ?? "", url: lists.url ?? "", minPrice: String(lists.minPrice ?? 0), labName: lists.labName ?? "", fasting: String(lists.fasting ?? 0), availableAt: String(lists.availableAt ?? 0), popular: lists.popular ?? "", category: lists.category ?? "", objectID: lists.objectID ?? "")
                sqliteDbStore.update(is_check: 1, itemId: lists.itemId ?? "" )
            }
           fetchToggleStatus()

            if checkArray.count > 0 {
                UIView.performWithoutAnimation {
                    self.view.layoutIfNeeded()
                }
            }
        }
        self.tableViews.reloadData()
        
    }
    
    @IBAction func TapToCart(_ sender: UIButton) {
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let home = storyBoard.instantiateViewController(withIdentifier: "CartViewController") as! CartViewController
        navigationController?.pushViewController(home, animated: true);
    }
    
}
extension ListViewController {
    
    func fetchList() {
        self.sqliteDbStore.deleteAllButton()
        self.sqliteDbStore.deleteAllList()
        viewModel.fetchListTest()
    }
    
    func fetchToggleStatus(){
        viewModel.fetchButtonStatus()

    }
    func prepareViewModelObserver() {
        self.viewModel.listDidChanges = { (finished, error) in
            
            if !error {
                print("error",error)
                print("finished",finished)
                 for items in self.viewModel.list {
                            print(items)
                            let model = addRemoveModel()
                            model.itemId = items.itemId!
                            model.is_check = false
                    self.sqliteDbStore.InsertAddRemoveButton(is_check: 0, itemId:items.itemId!)
                            self.userName.add(model)
                        }
                self.reloadTableView()
               
            }
        }
    }
    
    func reloadTableView() {
        self.tableViews.reloadData()
    }
}
extension ListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredTableData.removeAll(keepingCapacity: false)
        let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", searchController.searchBar.text!)
        let array = (viewModel.list as NSArray).contains(searchPredicate)
        filteredTableData = array as! [ListTest]
        self.tableViews.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(viewModel.list.count)
        var rowCount = 0
        if self.resultSearchController.isActive {
            rowCount = self.filteredTableData.count
        }else{
            rowCount = viewModel.list.count
        }
        return  rowCount
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: ListTableCell = tableView.dequeueReusableCell(withIdentifier: "ListTableCell", for: indexPath as IndexPath) as? ListTableCell else {
            fatalError("AddressCell cell is not found")
        }
        var id: String? = nil
        if self.resultSearchController.isActive {
            let lists = filteredTableData[indexPath.row]
            cell.Item = lists
            id = lists.itemId
        } else {
            let lists = viewModel.list[indexPath.row]
            cell.Item = lists
            id = lists.itemId
        }
       
        
        let model = self.userName[indexPath.row] as! addRemoveModel
       
        if (model.is_check) {
            cell.addRemoveButton.setTitle("Remove", for: .normal)
        }else {
            cell.addRemoveButton.setTitle("Add", for: .normal)
        }
     
        cell.addRemoveButton.tag = indexPath.row
        cell.addRemoveButton.addTarget(self, action: #selector(self.btnAddAction(_:)), for: .touchUpInside)
        
        return cell
    }
}

extension ListViewController : UISearchBarDelegate{
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if viewModel.list.count > 0 {
            filteredTableData = viewModel.list.filter{
                list in
                let id = list.itemId
                let name = list.itemName
                return id!.localizedCaseInsensitiveContains(searchText) == true || name!.localizedCaseInsensitiveContains(searchText) == true
            }
           
            print(filteredTableData)
            tableViews.reloadData()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        resultSearchController.isActive = false
        searchBar.text = nil
        searchBar.resignFirstResponder()
        tableViews.reloadData()
        
    }
}
