//
//  HTTPMethods.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation

enum HTTPMethods: String {
    
    case post = "POST"
    case get = "GET"
}
