//
//  APIService.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation
class APIService: NSObject, Requestable {

    static let instance = APIService()
    
    public var movies: [ListTest]?
   
    
    func fetchListTests(callback: @escaping Handler) {
        
        request(method: .get, url: Domain.baseUrl() + APIEndpoint.getlist) { (result) in
            
           callback(result)
        }
        
    }
    
}
