//
//  Client.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation


/// ENDPOINT CONFORMANCE
enum ListFeed {
    case testList
}

extension ListFeed: Endpoint {
    
    var base: String {
        return "https://5f1a8228610bde0016fd2a74.mockapi.io"
    }
    
    var path: String {
        switch self {
        case .testList: return "/getTestList"
        }
    }
}


struct ApiClient : GenericAPIClient{
     static let instance = ApiClient()
    internal let session: URLSession
       
       init(configuration: URLSessionConfiguration) {
           self.session = URLSession(configuration: configuration)
       }
       
    init() {
        self.init(configuration: .default)
    }
    
    typealias JSON = [String: Any]
       typealias JSONTaskCompletionHandler = (Result<JSON, APIError>) -> ()
    
    func fetchTestList(reuest: URLRequest ,completion: @escaping (Result<ListTest?, APIError>) -> ()){
    fetch(with: request , decode: { json -> ListTest? in
        guard let results = json as? ListTest else { return  nil }
        print("results:----",results)
        return results
    }, completion: completion)
    }
}
