//
//  Result.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation

enum Result<T, U> where U: Error  {
    case success(T)
    case failure(U)
}
