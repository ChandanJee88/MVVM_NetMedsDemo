//
//  CartTableCell.swift
//  NetMedsDemo
//
//  Created by Maa on 05/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import UIKit

class CartTableCell: UITableViewCell {
    @IBOutlet weak var titleId: UILabel!
    @IBOutlet weak var RemoveButton: UIButton!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var Amount: UILabel!
    let cartModel = CartViewModel()
    
    var Item = CartViewModel().listCart {
               didSet {
                   
                if let list = Item as? [String: Any]{
                    self.titleId.text = list["itemId"] as! String
                    self.titleName.text = list["itemName"] as? String
                    let amount:String = (list["minPrice"] as? String)!
     
                    self.Amount.text = amount

                   }
               }
           }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
