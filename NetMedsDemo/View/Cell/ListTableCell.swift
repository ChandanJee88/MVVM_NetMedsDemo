//
//  ListTableCell.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import UIKit

class ListTableCell: UITableViewCell {
    @IBOutlet weak var titleId: UILabel!
    @IBOutlet weak var addRemoveButton: UIButton!
    @IBOutlet weak var titleName: UILabel!
    @IBOutlet weak var Amount: UILabel!

    var Item: ListTest? {
           didSet {
               
               if let list = Item {
                   self.titleId.text = list.itemId ?? ""
                   self.titleName.text = list.itemName ?? ""
                let amount:String = String(list.minPrice ?? 0)
 
                self.Amount.text = amount

               }
           }
       }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
