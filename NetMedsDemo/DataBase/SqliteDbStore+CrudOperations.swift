//
//  SqliteDbStore+CrudOperations.swift
//  NetMedsDemo
//
//  Created by Maa on 04/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation
import SQLite3
extension SqliteDbStore {
    
    func insertDataList(Sno: String,itemId: String,itemName: String,type:String,Keyword: String,Best_sellers:String,testCount:String,IncludedTests:String,url:String,minPrice:String,labName:String,fasting:String,availableAt:String,popular:String,category:String,objectID:String) {
        // ensure statements are created on first usage if nil
        guard self.prepareInsertEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.insertEntryStmt)
        }
        
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Inserting id in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryStmt, 1, (Sno as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        //Inserting email in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryStmt, 2, (itemId as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        //Inserting password in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryStmt, 3, (itemName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        
        //Inserting mobile in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryStmt, 4, (type as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        
        if sqlite3_bind_text(self.insertEntryStmt, 5, (Keyword as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 6, (Best_sellers as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 7, (testCount as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 8, (IncludedTests as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 9, (url as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 10, (minPrice as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 11, (labName as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 12, (fasting as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 13, (availableAt as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 14, (popular as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 15, (category as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryStmt)")
            return
        }
        if sqlite3_bind_text(self.insertEntryStmt, 16, (objectID as NSString).utf8String, -1, nil) != SQLITE_OK {
                   logDbErr("sqlite3_bind_text(insertEntryStmt)")
                   return
               }
        //executing the query to insert values
        let r = sqlite3_step(self.insertEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(insertEntryStmt) \(r)")
            return
        }
    }
   
    func prepareInsertEntryStmt() -> Int32 {
        guard insertEntryStmt == nil else { return SQLITE_OK }
        let sql = "INSERT INTO Records (Sno, itemId, itemName, type, Keyword, Best_sellers, testCount, IncludedTests, url, minPrice, labName, fasting, availableAt, popular, category, objectID) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)"
        //preparing the query
        print("SQLLLLL Insert Records =====",sql)
        let r = sqlite3_prepare(db, sql, -1, &insertEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare insertEntryStmt")
        }
        return r
    }
    
    //MARK:- Insert Toggle Data
    func InsertAddRemoveButton(is_check: Int, itemId:String) {
        // ensure statements are created on first usage if nil
        guard self.prepareInsertButtonEntryStatemtent() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.insertEntryButtonStmt)
        }
                
        //Inserting is_check in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.insertEntryButtonStmt, 1, Int32(is_check)) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryButtonStmt)")
            return
        }
        //Inserting itemId in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.insertEntryButtonStmt, 2, (itemId as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(insertEntryButtonStmt)")
            return
        }
        
        //executing the query to insert values
        let r = sqlite3_step(self.insertEntryButtonStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(insertEntryButtonStmt) \(r)")
            return
        }
    }
    
    func prepareInsertButtonEntryStatemtent() -> Int32 {
           guard insertEntryButtonStmt == nil else { return SQLITE_OK }
           let sql = "INSERT INTO AddCart (is_check, itemId) VALUES (?,?)"
           //preparing the query
           print("SQLLLLLAddCart=====",sql)
           let r = sqlite3_prepare(db, sql, -1, &insertEntryButtonStmt, nil)
           if  r != SQLITE_OK {
               logDbErr("sqlite3_prepare insertEntryButtonStmt")
           }
           return r
       }
    
    //MARK:- Fetch All Data of Cart.
     //"SELECT * FROM Records
        func readDataList() throws -> Array<Dictionary<String,Any>> {
            // ensure statements are created on first usage if nil
            guard self.prepareReadEntryStmt() == SQLITE_OK else { throw SqliteError(message: "Error in prepareReadEntryStmt") }
            var result:Array<Dictionary<String,String>>=[]

            defer {
                // reset the prepared statement on exit.
                sqlite3_reset(self.readEntryStmt)
            }
            var stmt:OpaquePointer? = nil
            let sql = "SELECT * FROM Records"
            let strExec = sql.cString(using: String.Encoding.utf8)

            if (self.prepareReadEntryStmt() == SQLITE_OK){
                if (sqlite3_prepare_v2(db, strExec! , -1, &stmt, nil) == SQLITE_OK){
                    while (sqlite3_step(stmt) == SQLITE_ROW){
                        var i:Int32=0
                        let icount:Int32=sqlite3_column_count(stmt)
                        var dict=Dictionary<String, String>()
                        while i < icount
                        {
                            let strF=sqlite3_column_name(stmt, i)
                            let strV = sqlite3_column_text(stmt, i)

                            let rFiled:String=String(cString: strF!)
                            let rValue:String=String(cString: strV!)
                            //let rValue=String(cString: UnsafePointer<Int8>(strV!))

                            dict[rFiled] = rValue

                            i += 1
                        }
                        result.insert(dict, at: result.count)
                    }
                    sqlite3_finalize(stmt)
                }
                sqlite3_close(db)
            }
         
            return result
            
        }
   
       // READ operation prepared statement
        func prepareReadEntryStmt() -> Int32 {
            guard readEntryStmt == nil else { return SQLITE_OK }
    //        let sql = "SELECT * FROM Records WHERE ids = ? LIMIT 1"
            let sql = "SELECT * FROM Records"
            //preparing the query
            let r = sqlite3_prepare(db, sql, -1, &readEntryStmt, nil)
            if  r != SQLITE_OK {
                logDbErr("sqlite3_prepare readEntryStmt")
            }
            return r
        }
    
    //MARK:- Read/Fetch AddCart Toggle
        //"SELECT * FROM Addcart
           func readAddCartList() throws -> Array<Dictionary<String,Any>> {
               // ensure statements are created on first usage if nil
               guard self.prepareReadButtonEntryStmt() == SQLITE_OK else { throw SqliteError(message: "Error in prepareReadEntryStmt") }
               var result:Array<Dictionary<String,String>> = []

               defer {
                   // reset the prepared statement on exit.
                   sqlite3_reset(self.readEntryStmt)
               }
               var stmt:OpaquePointer? = nil
               let sql = "SELECT * FROM AddCart"
               let strExec = sql.cString(using: String.Encoding.utf8)

               if (self.prepareReadEntryStmt() == SQLITE_OK){
                   if (sqlite3_prepare_v2(db, strExec! , -1, &stmt, nil) == SQLITE_OK){
                       while (sqlite3_step(stmt) == SQLITE_ROW){
                           var i:Int32=0
                           let icount:Int32=sqlite3_column_count(stmt)
                           var dict=Dictionary<String, String>()
                           while i < icount
                           {
                               let strF=sqlite3_column_name(stmt, i)
                               let strV = sqlite3_column_text(stmt, i)

                               let rFiled:String=String(cString: strF!)
                               let rValue:String=String(cString: strV!)

                               dict[rFiled] = rValue

                               i += 1
                           }
                           result.insert(dict, at: result.count)
                       }
                       sqlite3_finalize(stmt)
                   }
                   sqlite3_close(db)
               }
               return result
           }
    
    // READ  addcart operation prepared statement
        func prepareReadButtonEntryStmt() -> Int32 {
            guard readEntryStmt == nil else { return SQLITE_OK }
            let sql = "SELECT * FROM AddCart"
            //preparing the query
            let r = sqlite3_prepare(db, sql, -1, &readEntryStmt, nil)
            if  r != SQLITE_OK {
                logDbErr("sqlite3_prepare readEntryStmt")
            }
            return r
        }
    
    //MARK:- Delete Cart list from DB
    //"DELETE FROM Records WHERE itemid = ?"
    func delete(itemId: String) {
        // ensure statements are created on first usage if nil
        guard self.prepareDeleteEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.deleteListEntryStmt)
        }
        
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Inserting name in deleteEntryStmt prepared statement
        if sqlite3_bind_text(self.deleteListEntryStmt, 1, (itemId as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(deleteListEntryStmt)")
            return
        }
        
        //executing the query to delete row
        let r = sqlite3_step(self.deleteListEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteListEntryStmt) \(r)")
            return
        }
    }
    
    // DELETE operation prepared statement
    func prepareDeleteEntryStmt() -> Int32 {
        guard deleteListEntryStmt == nil else { return SQLITE_OK }
        let sql = "DELETE FROM Records WHERE itemId = ?"
        //preparing the query
        let r = sqlite3_prepare(db, sql, -1, &deleteListEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare deleteListEntryStmt")
        }
        return r
    }
    
    //MARK:- Delete AddCart Toogle on First screen
    func deleteButton(itemId: String) {
        // ensure statements are created on first usage if nil
        guard self.prepareDeleteButtonEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.deleteButtonEntryStmt)
        }
                
        //Inserting name in deleteEntryStmt prepared statement
        if sqlite3_bind_text(self.deleteButtonEntryStmt, 1, (itemId as NSString).utf8String, -1, nil) != SQLITE_OK {
            logDbErr("sqlite3_bind_text(deleteButtonEntryStmt)")
            return
        }
        //executing the query to delete row
        let r = sqlite3_step(self.deleteEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteButtonEntryStmt) \(r)")
            return
        }
    }
    // DELETE operation prepared statement
    func prepareDeleteButtonEntryStmt() -> Int32 {
        guard deleteButtonEntryStmt == nil else { return SQLITE_OK }
        let sql = "DELETE FROM AddCart WHERE itemId = ?"
        //preparing the query
        let r = sqlite3_prepare(db, sql, -1, &deleteButtonEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare deleteButtonEntryStmt")
        }
        return r
    }
    
    //MARK:- Update Add Remove
    //"UPDATE Records SET is_check = ? WHERE itemId = ?"
    func update(is_check: Int, itemId:String) {
        // ensure statements are created on first usage if nil
        guard self.prepareUpdateEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.updateEntryStmt)
        }
        
       
        //  At some places (esp sqlite3_bind_xxx functions), we typecast String to NSString and then convert to char*,
        // ex: (eventLog as NSString).utf8String. This is a weird bug in swift's sqlite3 bridging. this conversion resolves it.
        
        //Inserting is_check in insertEntryStmt prepared statement
        if sqlite3_bind_int(self.updateEntryStmt, 1, Int32(is_check)) != SQLITE_OK {
                   logDbErr("sqlite3_bind_text(updateEntryStmt)")
                   return
        }
        
        //Inserting itemId in insertEntryStmt prepared statement
        if sqlite3_bind_text(self.updateEntryStmt, 2, (itemId as NSString).utf8String, -1, nil) != SQLITE_OK {
                   logDbErr("sqlite3_bind_text(updateEntryStmt)")
                   return
        }
       
        
        //executing the query to update values
        let r = sqlite3_step(self.updateEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(updateEntryStmt) \(r)")
            return
        }
    }
    // UPDATE operation prepared statement
       func prepareUpdateEntryStmt() -> Int32 {
           guard updateEntryStmt == nil else { return SQLITE_OK }
           let sql = "UPDATE AddCart SET is_check = ? WHERE itemId = ?"
        print("Update AddCart =====",sql)
           //preparing the query
           let r = sqlite3_prepare(db, sql, -1, &updateEntryStmt, nil)
           if  r != SQLITE_OK {
               logDbErr("sqlite3_prepare updateEntryStmt")
           }
           return r
       }
    //MARK:- Delete All data of Add remove
    func deleteAllButton() {
        // ensure statements are created on first usage if nil
        guard self.prepareDeleteButtonAllEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.deleteEntryStmt)
        }
                
        //executing the query to delete row
        let r = sqlite3_step(self.deleteEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteEntryStmt) \(r)")
            return
        }
    }
    // DELETE operation prepared statement
       func prepareDeleteButtonAllEntryStmt() -> Int32 {
           guard deleteEntryStmt == nil else { return SQLITE_OK }
           let sql = "DELETE FROM AddCart"
        print("Delete AddCart =====",sql)
           //preparing the query
           let r = sqlite3_prepare(db, sql, -1, &deleteEntryStmt, nil)
           if  r != SQLITE_OK {
               logDbErr("sqlite3_prepare deleteEntryStmt")
           }
           return r
       }
    
    func deleteAllList() {
        // ensure statements are created on first usage if nil
        guard self.prepareDeleteAllEntryStmt() == SQLITE_OK else { return }
        
        defer {
            // reset the prepared statement on exit.
            sqlite3_reset(self.deleteAllEntryStmt)
        }
                
        //executing the query to delete row
        let r = sqlite3_step(self.deleteAllEntryStmt)
        if r != SQLITE_DONE {
            logDbErr("sqlite3_step(deleteEntryStmt) \(r)")
            return
        }
    }
    // DELETE operation prepared statement
    func prepareDeleteAllEntryStmt() -> Int32 {
        guard deleteAllEntryStmt == nil else { return SQLITE_OK }
        let sql = "DELETE FROM Records"
     print("Delete Records =====",sql)
        //preparing the query
        let r = sqlite3_prepare(db, sql, -1, &deleteAllEntryStmt, nil)
        if  r != SQLITE_OK {
            logDbErr("sqlite3_prepare deleteEntryStmt")
        }
        return r
    }
}
