//
//  SqliteDbStore.swift
//  NetMedsDemo
//
//  Created by Maa on 04/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation
import SQLite3
import os.log

class SqliteDbStore {
    // Get the URL to db store file
    let dbURL: URL
    // The database pointer.
    var db: OpaquePointer?
//    var dbADD: OpaquePointer?
    // Prepared statement https://www.sqlite.org/c3ref/stmt.html to insert an event into Table.
    // we use prepared statements for efficiency and safe guard against sql injection.
    var insertEntryStmt: OpaquePointer?
    var insertEntryButtonStmt: OpaquePointer?
    var readEntryStmt: OpaquePointer?
    var updateEntryStmt: OpaquePointer?
    var deleteEntryStmt: OpaquePointer?
    var deleteButtonEntryStmt: OpaquePointer?
     var deleteListEntryStmt: OpaquePointer?
    var deleteAllEntryStmt: OpaquePointer?
    
    let oslog = OSLog(subsystem: "NetMedsDemo", category: "sqliteintegration")

    init() {
        do {
            do {
                dbURL = try FileManager.default
                    .url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                    .appendingPathComponent("integration.db")
                os_log("URL:- %s", dbURL.absoluteString)
            } catch {
                //TODO: Just logging the error and returning empty path URL here. Handle the error gracefully after logging
                os_log("Some error occurred. Returning empty path.")
                dbURL = URL(fileURLWithPath: "")
                return
            }
            
            try openDB()
            try createAddRemoveTables()
            try createListTables()
        } catch {
            //TODO: Handle the error gracefully after logging
            os_log("Some error occurred. Returning.")
            return
        }
    }
    
    // Command: sqlite3_open(dbURL.path, &db)
    // Open the DB at the given path. If file does not exists, it will create one for you
    func openDB() throws {
        if sqlite3_open(dbURL.path, &db) != SQLITE_OK { // error mostly because of corrupt database
            os_log("error opening database at %s", log: oslog, type: .error, dbURL.absoluteString)
            //            deleteDB(dbURL: dbURL)
            throw SqliteError(message: "error opening database \(dbURL.absoluteString)")
        }
    }
    
    // Code to delete a db file. Useful to invoke in case of a corrupt DB and re-create another
    func deleteDB(dbURL: URL) {
        os_log("removing db", log: oslog)
        do {
            try FileManager.default.removeItem(at: dbURL)
        } catch {
            os_log("exception while removing db %s", log: oslog, error.localizedDescription)
        }
    }
    
    func createAddRemoveTables() throws {
        // create the tables if they dont exist.
        
        // create the table to store the entries.
        let ret =  sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS AddCart (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, is_check INTEGER NOT NULL, itemId TEXT NOT NULL)", nil, nil, nil)

        if (ret != SQLITE_OK) { // corrupt database.
            logDbErr("Error creating db table - AddCart")
            throw SqliteError(message: "unable to create table AddCart")
        }
        
    }
    
    
    func createListTables() throws {
        // create the tables if they dont exist.
        
        // create the table to store the entries.
        // ID | Sno | itemId | itemName
        let ret =  sqlite3_exec(db, "CREATE TABLE IF NOT EXISTS Records (id INTEGER UNIQUE PRIMARY KEY AUTOINCREMENT, Sno TEXT NOT NULL, itemId TEXT NOT NULL, itemName TEXT NOT NULL, type TEXT NOT NULL, Keyword TEXT NOT NULL, Best_sellers TEXT NOT NULL, testCount TEXT NOT NULL, IncludedTests TEXT NOT NULL, url TEXT NOT NULL, minPrice TEXT NOT NULL, labName TEXT NOT NULL, fasting TEXT NOT NULL, availableAt TEXT NOT NULL, popular TEXT NOT NULL, category TEXT NOT NULL, objectID TEXT NOT NULL)", nil, nil, nil)

        if (ret != SQLITE_OK) { // corrupt database.
            logDbErr("Error creating db table - Records")
            throw SqliteError(message: "unable to create table Records")
        }
        
    }
    func logDbErr(_ msg: String) {
        let errmsg = String(cString: sqlite3_errmsg(db)!)
        os_log("ERROR %s : %s", log: oslog, type: .error, msg, errmsg)
    }
}

// Indicates an exception during a SQLite Operation.
class SqliteError : Error {
    var message = ""
    var error = SQLITE_ERROR
    init(message: String = "") {
        self.message = message
    }
    init(error: Int32) {
        self.error = error
    }
}
