//
//  CartViewModel.swift
//  NetMedsDemo
//
//  Created by Maa on 05/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation

protocol CartViewModelProtocol {
    func fethDataFromDB()
}
class CartViewModel :CartViewModelProtocol{
    let sqliteDbStore = SqliteDbStore()
    var cartList = Array<Dictionary<String,Any>>()
    var  listCart = [String: Any]()
    
    func fethDataFromDB(){
        guard let list = try? sqliteDbStore.readDataList() else { return }
        cartList.removeAll()
        cartList.append(contentsOf: list)
        print(cartList)
    }
}
