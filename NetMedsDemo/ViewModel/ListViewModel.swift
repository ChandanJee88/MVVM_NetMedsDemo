//
//  ListViewModel.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation
protocol ListViewModelProtocol {
    
    var listDidChanges: ((Bool, Bool) -> Void)? { get set }
    
    func fetchListTest()
    func fetchButtonStatus()
}

class ListViewModel: ListViewModelProtocol{
    let sqliteDbStore = SqliteDbStore()
    var cartListToggle = Array<Dictionary<String,Any>>()

    //MARK: - Internal Properties
    
    var listDidChanges: ((Bool, Bool) -> Void)?
    
    var list = [ListTest]() {
        //     var list: [ListBase]? {
        didSet {
            print("lists",list.count as Any)
            self.listDidChanges!(true, false)
        }
    }
    
    func fetchButtonStatus(){
        guard let list = try? sqliteDbStore.readAddCartList() else { return }
        cartListToggle.removeAll()
        if list.count > 0{
        cartListToggle.append(contentsOf: list)
        }
        print("Toogle List:- ",cartListToggle.count)
    }
    
    func fetchListTest(){
        APIService.instance.fetchListTests{
            result in
            switch result{
            case .success(let data):
                do{
                    // make sure this JSON is in the format we expect
                    if let json = try JSONSerialization.jsonObject(with: data, options: [.mutableContainers]) as? [[String: Any]] {
                        
                        for arrJson in json{
                            let dic = ListTest(arrJson)
                            self.list.append(dic)
                        }
                        print("Total List",self.list.count)
                    }
                } catch let error as NSError {
                    print("Failed to load: \(error.localizedDescription)")
                }
                
                break
            case .failure(let error):
                
                print(error.description)
            }
        }
    }
}
