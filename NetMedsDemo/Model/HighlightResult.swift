//
//  HighlightResult.swift
//
//  Created on August 04, 2020
//
import Foundation

struct HighlightResult {

	var itemName: ItemName?
	var Keywords: Keyword?
	var IncludedTestss: IncludedTests?
	var category: Category?
    
    init(_ dict: [String: Any]) {

		if let itemNameDict = dict["itemName"] as? [String: Any] {
			itemName = ItemName(itemNameDict)
		}

		if let KeywordDict = dict["Keyword"] as? [String: Any] {
			Keywords = Keyword(KeywordDict)
		}

		if let IncludedTestsDict = dict["Included Tests"] as? [String: Any] {
			IncludedTestss = IncludedTests(IncludedTestsDict)
		}

		if let categoryDict = dict["category"] as? [String: Any] {
			category = Category(categoryDict)
		}
	}

	func toDictionary() -> [String: Any] {
		var jsonDict = [String: Any]()
		jsonDict["itemName"] = itemName?.toDictionary()
		jsonDict["Keyword"] = Keywords?.toDictionary()
		jsonDict["Included Tests"] = IncludedTestss?.toDictionary()
		jsonDict["category"] = category?.toDictionary()
		return jsonDict
	}

}
