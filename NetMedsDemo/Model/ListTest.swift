//
//  ListTest.swift
//
//  Created on August 02, 2020
//

import Foundation

struct ListTest {

	let Sno: Int?
	let itemId: String?
	let itemName: String?
	let type: String?
	let Keyword: String?
	let Best_sellers: String?
	let testCount: Int?
	let IncludedTests: String?
	let url: String?
	let minPrice: Int?
	let labName: String?
	let fasting: Int?
	let availableAt: Int?
	let popular: String?
	let category: String?
	let objectID: String?
	var HighlightResults: HighlightResult?

	init(_ dict: [String: Any]) {
		Sno = dict["S.no"] as? Int
		itemId = dict["itemId"] as? String
		itemName = dict["itemName"] as? String
		type = dict["type"] as? String
		Keyword = dict["Keyword"] as? String
		Best_sellers = dict["Best-sellers"] as? String
		testCount = dict["testCount"] as? Int
		IncludedTests = dict["Included Tests"] as? String
		url = dict["url"] as? String
		minPrice = dict["minPrice"] as? Int
		labName = dict["labName"] as? String
		fasting = dict["fasting"] as? Int
		availableAt = dict["availableAt"] as? Int
		popular = dict["popular"] as? String
		category = dict["category"] as? String
		objectID = dict["objectID"] as? String

        if let HighlightResultDict = dict["_highlightResult"] as? [String: Any] {
            HighlightResults = HighlightResult(HighlightResultDict)
		}
	}

	func toDictionary() -> [String: Any] {
		var jsonDict = [String: Any]()
		jsonDict["S.no"] = Sno
		jsonDict["itemId"] = itemId
		jsonDict["itemName"] = itemName
		jsonDict["type"] = type
		jsonDict["Keyword"] = Keyword
		jsonDict["Best-sellers"] = Best_sellers
		jsonDict["testCount"] = testCount
		jsonDict["Included Tests"] = IncludedTests
		jsonDict["url"] = url
		jsonDict["minPrice"] = minPrice
		jsonDict["labName"] = labName
		jsonDict["fasting"] = fasting
		jsonDict["availableAt"] = availableAt
		jsonDict["popular"] = popular
		jsonDict["category"] = category
		jsonDict["objectID"] = objectID
        jsonDict["_highlightResult"] = HighlightResults?.toDictionary()
		return jsonDict
	}

}

