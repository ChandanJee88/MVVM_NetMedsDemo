//
//  ListTest.swift
//  NetMedsDemo
//
//  Created by Maa on 02/08/20.
//  Copyright © 2020 Chandan Jee. All rights reserved.
//

import Foundation
import UIKit
//import SwiftyJSON

/*
struct ListTest {
    var Sno : String
    var itemId: String
    var objectID: String
    var category : String
    var popular: Bool
    var availableAt: String
    var fasting: String
    var itemName: String
    var url: String
    var type: String
    var highlightResult: highlightResult
    var summary: String
    var labName: String
    var minPrice: String
    var IncludedTests : String
    var testCount: String
    var Best_sellers : String
    var Keyword : String
}

struct highlightResult {
    
    var itemName: ResultVal
    var Keyword: ResultVal
    var IncludedTests : ResultVal
    var category : ResultVal
    
    init(jsonHighLight: JSON) {
        self.itemName = jsonHighLight["itemName"].stringValue
        self.Keyword = jsonHighLight["Keyword"].stringValue
        self.IncludedTests = jsonHighLight["Included Tests"].stringValue
        self.category = jsonHighLight["category"].stringValue
    }
}

struct ResultVal {
    var value: String
    var matchLevel: String
    var matchedWords : [String]
    
    init(jsondata:JSON) {
         self.value = jsondata["value"].stringValue
        self.matchLevel = jsondata["matchLevel"].stringValue
         self.matchedWords = [String]()
        let matched = jsondata["matchedWords"].arrayValue
        for Words in matched
        {
            self.matchedWords.append(Words.description)
            
        }
    }
}

extension ListTest{
    struct Key {
        static let Sno = "S.no"
          static let itemId = "itemId"
          static let objectID = "objectID"
          static let category = "category"
          static let popular = "popular"
          static let availableAt =  "availableAt"
         static  let fasting = "fasting"
          static let itemName = "itemName"
          static let url = "url"
          static let type = "type"
           static let _highlightResult = "_highlightResult"
          static let labName = "labName"
          static let minPrice = "minPrice"
           static let IncludedTests = "Included Tests"
          static let testCount = "testCount"
          static let Best_sellers = "Best-sellers"
          static let Keyword = "Keyword"
    }
    
    init(jsonData: JSON) {
        self.Sno = jsonData[Sno].stringValue
        self.itemId = jsonData[itemId].stringValue
        self.objectID = jsonData[objectID].stringValue
        self.category = jsonData[category].stringValue
        self.popular = jsonData[popular].stringValue
        self.availableAt = jsonData[availableAt].stringValue
        self.fasting = jsonData[fasting].stringValue
        self.itemName = jsonData[itemName].stringValue
        self.url = jsonData[url].stringValue
        self.type = jsonData[type].stringValue
        
        self.labName = jsonData[labName].stringValue
        self.minPrice = jsonData[minPrice].stringValue
        self.IncludedTests = jsonData[IncludedTests].stringValue
        self.testCount = jsonData[testCount].stringValue
        self.Best_sellers = jsonData[Best_sellers].stringValue
        self.Keyword = jsonData[Keyword].stringValue
        
        self.highlightResult = [highlightResult]()

        let highLight = jsonData[_highlightResult].arrayValue
        for list in highLight
        {
           let value = VideoData(jsondata: list)
            self.highlightResult.append(value)
        }
    }
}
*/
/*
struct ListTest: Codable {
    var Sno : String?
    var itemId: String?
    var objectID: String?
    var category : String?
    var popular: Bool?
    var availableAt: String?
    var fasting: String?
    var itemName: String?
    var url: String?
    var type: String?
    var highlightResult: [highLightResults]?
    var summary: String?
    var labName: String?
    var minPrice: String?
    var IncludedTests : String?
    var testCount: String?
    var Best_sellers : String?
    var Keyword : String?
    
     enum CodingKeys: String, CodingKey {
        case  Sno = "S.no"
         case  itemId = "itemId"
         case  objectID = "objectID"
         case  category = "category"
         case  popular = "popular"
         case  availableAt =  "availableAt"
        case   fasting = "fasting"
         case  itemName = "itemName"
         case  url = "url"
         case  type = "type"
          case  highlightResult = "_highlightResult"
         case  labName = "labName"
         case  minPrice = "minPrice"
          case  IncludedTests = "Included Tests"
         case  testCount = "testCount"
         case  Best_sellers = "Best-sellers"
         case  Keyword = "Keyword"
    }
    
    init(from decoder: Decoder) throws {
    let values = try decoder.container(keyedBy: CodingKeys.self)
        Sno = try values.decodeIfPresent(String.self, forKey: .Sno)
        itemId = try values.decodeIfPresent(String.self, forKey: .itemId)
        objectID = try values.decodeIfPresent(String.self, forKey: .objectID)
        category = try values.decodeIfPresent(String.self, forKey: .category)
        popular = try values.decodeIfPresent(Bool.self, forKey: .popular)
        availableAt = try values.decodeIfPresent(String.self, forKey: .availableAt)
        fasting = try values.decodeIfPresent(String.self, forKey: .fasting)
        itemName = try values.decodeIfPresent(String.self, forKey: .itemName)
        url = try values.decodeIfPresent(String.self, forKey: .url)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        highlightResult = try values.decodeIfPresent([highLightResults].self, forKey: .highlightResult)
        labName = try values.decodeIfPresent(String.self, forKey: .labName)
        minPrice = try values.decodeIfPresent(String.self, forKey: .minPrice)
        IncludedTests = try values.decodeIfPresent(String.self, forKey: .IncludedTests)
        testCount = try values.decodeIfPresent(String.self, forKey: .testCount)
        Best_sellers = try values.decodeIfPresent(String.self, forKey: .Best_sellers)
        Keyword = try values.decodeIfPresent(String.self, forKey: .Keyword)
//         highlightResult = try values.decodeIfPresent(highLightResults.self, forKey: .highlightResult)!
//        highlightResult?.append(contentsOf: tempHobbyList)
    }
}

    
struct highLightResults :  Codable {
    
    var itemName: [ResultVal]?
    var Keyword: [ResultVal]?
    var IncludedTests : [ResultVal]?
    var category : [ResultVal]?
    
    enum CodingKeyss: String, CodingKey {
        case itemNames = "itemName"
        case Keywords = "Keyword"
        case IncludedTestss = "Included Tests"
        case categorys = "category"
    }
    
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeyss.self)
           itemName = try values.decodeIfPresent([ResultVal].self, forKey: .itemNames)
         Keyword = try values.decodeIfPresent([ResultVal].self, forKey: .Keywords)
         IncludedTests = try values.decodeIfPresent([ResultVal].self, forKey: .IncludedTestss)
         category = try values.decodeIfPresent([ResultVal].self, forKey: .categorys)
        
    }
   
}

struct ResultVal : Codable{
    
    var value: String?
    var matchLevel: String?
    var matchedWords : [String]?
    
    enum CodingKeys: String, CodingKey {
        case value = "value"
        case matchLevel = "matchLevel"
        case matchedWords = "matchedWords"
    }
    init(from decoder: Decoder) throws {
       let values = try decoder.container(keyedBy: CodingKeys.self)
        value = try values.decodeIfPresent(String.self, forKey: .value)
         matchLevel = try values.decodeIfPresent(String.self, forKey: .matchLevel)
         matchedWords = try values.decodeIfPresent([String].self, forKey: .matchedWords)
        
    }
    
}
*/
// MARK: Array extension

extension Array where Element == ListTest {
    
    func matching(_ query: String) -> [ListTest] {
        
        return self.filter({ $0.Keyword == query || $0.itemName == query})
    }
    
    func sortByName() -> [ListTest] {
        
        return self.sorted(by: { (first, next) -> Bool in
            
            return first.itemName.compare(next.itemName) == .orderedAscending
        })
    }
}


