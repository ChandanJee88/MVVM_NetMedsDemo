//
//  Category.swift
//
//  Created on August 04, 2020
//
import Foundation

struct Category {

	let value: String?
	let matchLevel: String?
	let matchedWords: [Any]?

	init(_ dict: [String: Any]) {
		value = dict["value"] as? String
		matchLevel = dict["matchLevel"] as? String
		matchedWords = dict["matchedWords"] as? [Any]
	}

	func toDictionary() -> [String: Any] {
		var jsonDict = [String: Any]()
		jsonDict["value"] = value
		jsonDict["matchLevel"] = matchLevel
		jsonDict["matchedWords"] = matchedWords
		return jsonDict
	}

}
